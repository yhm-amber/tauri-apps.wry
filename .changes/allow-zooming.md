---
"wry": minor
---

Add option to enable/disable zoom shortcuts for WebView2, disabled by default.